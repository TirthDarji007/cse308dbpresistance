import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.*;

public class Utilities {
    /**
     * From the given point JSON it returns the longitude and latitude in an array.
     * @param pointJSON
     * @return
     */
    public static double[] getCenterCoordinates(String pointJSON){
        double[] arr = new double[2];
        try {
            JSONObject jo = new JSONObject(pointJSON);
            // To get center point from industry standard pointJSON.
            //Ex: {"type": "Feature","geometry": {"type": "Point","coordinates": [125,10]},"properties": {"name": "ActualJSON"}}
            /*
            JSONObject geometry = jo.getJSONObject("geometry");
            JSONArray corArray = geometry.getJSONArray("coordinates");
            double longiutde = corArray.getDouble(0);
            double latitude = corArray.getDouble(1);
            */

            // TO get information from cse308 precinct JSON
            // Ex: {"x":-94.64482,"y":41.19768}
            double longiutde = jo.getDouble("x");
            double latitude = jo.getDouble("y");

            arr[0] = longiutde;
            arr[1] = latitude;
        }catch (JSONException e){
            e.printStackTrace();
        }
        return arr;
    }

    public static double distanceJSONPoint(String pointJSON1, String pointJSON2){
        double[] arr1 = getCenterCoordinates(pointJSON1);
        double[] arr2 = getCenterCoordinates(pointJSON2);
        double distance = Math.sqrt(Math.pow((arr1[0]-arr2[0]),2)+Math.pow((arr1[1]-arr2[1]),2));
        return distance;
    }

    // function to sort hashmap by values
    public static HashMap<Integer, Double> sortByValue(HashMap<Integer, Double> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<Integer, Double> > list =
                new LinkedList<Map.Entry<Integer, Double> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<Integer, Double> >() {
            public int compare(Map.Entry<Integer, Double> o1,
                               Map.Entry<Integer, Double> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<Integer, Double> temp = new LinkedHashMap<Integer,Double>();
        for (Map.Entry<Integer,Double> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    private static String moveJson(int precinctID, int fromDistrictID, int toDistrictID){
        try {
            JSONObject jo = new JSONObject();
            jo.put("type", "Move");
            jo.put("precinctID", precinctID);
            jo.put("fromDistrictID", fromDistrictID);
            jo.put("toDistrictID", toDistrictID);
            return jo.toString();
        }catch (JSONException e){
            //e.printStackTrace();
            return "{" +
                    "\"type\":\"Move\","+
                    "\"precinctID\":\"" + precinctID + "\""+
                    ",\"fromDistrictID\":\"" + fromDistrictID +"\""+
                    ",\"toDistrictID\":\"" + toDistrictID + "\""+
                    '}';
        }
    }

    /**
     * @param moveList Each of the list item should be valid JSON. Use move object toString method to generate it.
     * @return the Single Json Object which contains all the moves by an algorithm.
     */
    public static String moveJsonList(List<String> moveList){
        try {
            JSONObject jo = new JSONObject();
            JSONObject temp;
            JSONArray ja = new JSONArray();
            for (String s : moveList) {
                temp = new JSONObject(s);
                ja.put(temp);
            }
            jo.put("moves",ja);
            return jo.toString();
        }catch (JSONException e){
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String [] args){
        String jstring = "{" + "\"type\": \"Feature\"," + "  \"geometry\": {" + "    \"type\": \"Point\"," +
                "    \"coordinates\": [0,0]" + "  }," + "  \"properties\": {" +
                "    \"name\": \"Dinagat Islands\"" +  "  }" +  "}";
        String jstring2 = "{" + "\"type\": \"Feature\"," + "  \"geometry\": {" + "    \"type\": \"Point\"," +
                "    \"coordinates\": [3, 4]" + "  }," + "  \"properties\": {" +
                "    \"name\": \"Dinagat Islands\"" +  "  }" +  "}";
        System.out.println(distanceJSONPoint(jstring,jstring2));

        HashMap<Integer,Double> hm = new HashMap<>();
        hm.put(1,5.0);
        hm.put(2,4.0);
        hm.put(3,3.0);
        hm.put(4,2.0);
        hm.put(5,1.0);
        HashMap<Integer, Double> hm1 = sortByValue(hm);

        // print the sorted hashmap
        for (Map.Entry<Integer,Double> en : hm1.entrySet()) {
            System.out.println("Key = " + en.getKey() +
                    ", Value = " + en.getValue());
        }

        System.out.println("keys"+ hm1.keySet());
        System.out.println("Values" + hm1.values());

        //moveList test
        System.out.println("Move test" + new Move(1,2,3).toString());
        List<String> moves = new LinkedList<>();
        for (int i=0; i<5; i++){
            moves.add(new Move(i,i+10,i+20).toString());
        }
        String movesJson = moveJsonList(moves);
        System.out.println(movesJson);
    }

    /**
     *
     * @param path - server absolute path location where all data is stored.
     * @param userid userid - userid. path/userid folder will be created.
     * @param generatedState
     */
    public static void saveGeneratedState(String path, Integer userid, GeneratedState generatedState){
        File pathfile = new File(path);
        if (!pathfile.exists()){
            pathfile.mkdirs();
        }
        String userPath = path+File.separator+userid;
        pathfile = new File(userPath);
        if (!pathfile.exists()){
            pathfile.mkdirs();
        }
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String filename = generatedState.getState().getName() + "_" + userid + "_" +
                cal.get(Calendar.YEAR)+(cal.get(Calendar.MONTH)+1)+cal.get(Calendar.DATE)+
                cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE);
        String fileloc = userPath + File.separator + filename;
        try {
            FileOutputStream fos = new FileOutputStream(fileloc);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(generatedState);
            out.close();
            fos.close();
            System.out.println("Object has been serialized");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Loads the saved generated state
     * @param filename
     * @return
     */
    public static GeneratedState loadGeneratedState(String filename){
        try {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);
            Object object = in.readObject();

            in.close();
            file.close();

            System.out.println("Object has been deserialized ");
            return (GeneratedState) object;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Saves the weight for user.
     * @param path - server absolute path location where all data is stored.
     * @param userid - userid. path/userid folder will be created.
     * @param populationPerc
     * @param compactnessPerc
     * @param politicalPrec
     */
    public static void saveWeights(String path, Integer userid, Integer populationPerc, Integer compactnessPerc, Integer politicalPrec){
        File pathfile = new File(path);
        if (!pathfile.exists()){
            pathfile.mkdirs();
        }
        String userPath = path+File.separator+userid;
        pathfile = new File(userPath);
        if (!pathfile.exists()){
            pathfile.mkdirs();
        }
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String filename = "Weight" + "_" + userid + "_" +
                cal.get(Calendar.YEAR)+(cal.get(Calendar.MONTH)+1)+cal.get(Calendar.DATE)+
                cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE);
        String fileloc = userPath + File.separator + filename;
        try {
            FileWriter fw = new FileWriter(userPath+ File.separator+ filename);
            fw.write(populationPerc+","+compactnessPerc+","+politicalPrec+"\n");
            fw.close();
            System.out.println("weights has been saved.");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param filename absolute file location where weights is stored
     * @return list of integer. order is following: population, compactness, population
     */
    public static List<Integer> loadWeights(String filename){
        try {
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            String[] vals = scanner.nextLine().split(",");
            List<Integer> weights = new LinkedList<>();
            for (String val: vals){
                weights.add(Integer.parseInt(val));
            }
            return weights;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
