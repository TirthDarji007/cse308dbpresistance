public class LoadDataThread implements Runnable {
    private StateID stateID;
    private volatile boolean running = true;
    private volatile boolean paused = false;
    private final Object pauseLock = new Object();

    public LoadDataThread(StateID stateID){
        this.stateID = stateID;
    }

    public StateID getStateID() {
        return stateID;
    }

    public void setStateID(StateID stateID) {
        this.stateID = stateID;
    }

    public void finish(){
        this.running = false;
    }

    @Override
    public void run() {
        int count = 0;
        while (running){
            synchronized (pauseLock) {
                if (!running) {
                    break;
                }
                if (paused) {
                    try {
                        pauseLock.wait();
                    } catch (InterruptedException ex) {
                        break;
                    }
                    if (!running) {
                        break;
                    }
                }
            }
            DataSingleton.getInstance().makeSureDataExist(this.stateID);
        }

    }

    public void resume() {
        synchronized (pauseLock) {
            paused = false;
            pauseLock.notifyAll(); // Unblocks thread
        }
    }

    public void stop() {
        running = false;
        resume();
    }

    public void pause() {
        paused = true;
    }

    public static void main(String [] args){
        LoadDataThread test = new LoadDataThread(StateID.NE);
        Thread mythread = new Thread(test);
        mythread.start();

    }
}
