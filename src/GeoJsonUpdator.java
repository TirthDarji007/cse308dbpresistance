import gerrymandering.HibernateManager;
import gerrymandering.model.District;
import gerrymandering.model.Precinct;
import gerrymandering.model.State;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GeoJsonUpdator {
    //private static final String WORK_PATH = System.getProperty("user.home") + File.separator;
    private static final String WORK_PATH = ".";
    private static final String PRECINCT_LOC = WORK_PATH + File.separator + "Precinct";


    private static void updateGeoJasonFiles(){

        //Variable to hold data.
        List<Object> statesList, distList, precList;
        Iterator<Object> itrState, itrDist, itrPrec;
        State state;
        District district;
        Precinct precinct;
        Map<String,Object> criteriaDist;
        Map<String,Object> criteriaPrec;
        File fileState;
        File fileDistrict;
        String stateLoc;
        String distLoc;
        String precLoc;
        PrintWriter writer;

        //Make sure precincts directory exists
        File filePrecinct = new File(PRECINCT_LOC);
        if (! filePrecinct.exists()){
            if (filePrecinct.mkdirs()){
                System.out.println("Precinct file created. " + PRECINCT_LOC);
            }else{
                System.out.println("Precinct folder creation failed. " + PRECINCT_LOC);
            }
        }

        try {
            HibernateManager hb = HibernateManager.getInstance();

            //Loop through each state
            statesList = hb.getAllRecords(State.class);
            itrState = statesList.iterator();
            while (itrState.hasNext()){
                state = (State) itrState.next();
                System.out.println(state.getStateId() + " " + state.getShortName() + " " + state.getName() + " " + state.getConstitutionText());

                //Make sure state directory exist
                //stateLoc = PRECINCT_LOC + File.separator + state.getName();
                stateLoc = PRECINCT_LOC + File.separator + state.getShortName();
                fileState = new File(stateLoc);
                if (! fileState.exists()){
                    if (fileState.mkdirs()){
                        System.out.println("State file created. " + fileState.getPath());
                    }else{
                        System.out.println("State folder creation failed. " + fileState.getPath());
                    }
                }

                // Get all the Districts
                criteriaDist = new HashMap<>();
                criteriaDist.put("stateId",state.getStateId());
                distList = hb.getRecordsBasedOnCriteria(District.class,criteriaDist);
                itrDist = distList.iterator();
                while (itrDist.hasNext()){
                    district = (District) itrDist.next();
                    System.out.println("\t" + district.getDistrictId() + " " + district.getStateId() + " " +
                            district.getName());

                    //Make sure district directory exist
                    //distLoc = PRECINCT_LOC + File.separator + state.getName() + File.separator + district.getDistrictId();
                    distLoc = PRECINCT_LOC + File.separator + state.getShortName() + File.separator + district.getDistrictId();
                    fileDistrict = new File(distLoc);
                    if (! fileDistrict.exists()){
                        if (fileDistrict.mkdirs()){
                            System.out.println("District file created. " + fileDistrict.getPath());
                        }else{
                            System.out.println("District folder creation failed. " + fileDistrict.getPath());
                        }
                    }

                    //Get all the precincts in
                    criteriaPrec = new HashMap<>();
                    criteriaPrec.put("districtId",district.getDistrictId());
                    precList = hb.getRecordsBasedOnCriteria(Precinct.class,criteriaPrec);
                    itrPrec = precList.iterator();
                    while (itrPrec.hasNext()){
                        precinct = (Precinct) itrPrec.next();
                        precLoc = distLoc + File.separator + precinct.getPrecinctId() + ".json";
                        System.out.println("\t\t Precinct Detail" + precinct.getPrecinctId() + " " + precinct.getDistrictId() + " ");
                        writer = new PrintWriter(precLoc);
                        writer.print(precinct.getBoundaryJSON());
                        writer.close();
                    }
                }

            }
        }
        catch (Exception e) {
            System.out.println("Exception ");
            e.printStackTrace();
        }
        catch (Throwable t){
            System.out.println("Throwable Exception ");
            t.printStackTrace();
        }



    }

    public void runUpdate(){
        updateGeoJasonFiles();
    }

    public static void main(String [] args){
        updateGeoJasonFiles();
    }
}

