import gerrymandering.model.District;
import gerrymandering.model.Precinct;

import java.util.HashMap;
import java.util.List;

public class ObjectiveFunction {

    /**
     * This method calculated the fairness of given state.
     * The state is taken from the database.
     * @param stateID
     * @return
     */
    public synchronized static double databaseStateFairness(StateID stateID){
        double populationFair = populationFairness(stateID);
        double compactFair = compactnessFairness(stateID);
        double policalFair = politicalFairness(stateID);
        return ((populationFair+compactFair+policalFair)/3);
    }

    /**
     * This method calculated the fairness of given state.
     * The state is taken from the database.
     * @param stateID
     * @return
     */
    public synchronized static double databaseStateFairness(StateID stateID,Integer populationPerc, Integer compactnessPerc, Integer politicalPrec){
        double populationFair = (populationFairness(stateID)) * (((double) populationPerc) / 100.0);
        double compactFair = (compactnessFairness(stateID)) * (((double) compactnessPerc) / 100.0);
        double policalFair = (politicalFairness(stateID)) * (((double) politicalPrec) / 100.0);
        return ((populationFair+compactFair+policalFair)/3.0);
    }


    private synchronized static double populationFairness(StateID stateID){
        double fairness = 0.0;
        DataSingleton data = DataSingleton.getInstance();
        int numOfDistricts = data.getDistricts(stateID).size();
        double statePopulation = data.populationState(stateID);
        double fairPopulation = statePopulation/numOfDistricts;
        double differenceTotal = 0.0;
        for (District d: data.getDistricts(stateID)) {
            differenceTotal += Math.abs(fairPopulation - data.populationDistrict(stateID,d.getDistrictId()));
        }
        double differenceAve = differenceTotal / numOfDistricts;
        fairness = 100 - ((differenceAve/fairPopulation)*100);
        return fairness;
    }

    private synchronized static double compactnessFairness(StateID stateID){
        double fairness = 0;
        DataSingleton data = DataSingleton.getInstance();
        int numOfDistricts = data.getDistricts(stateID).size();
        List<Precinct> precinctList;
        int demWins, repWins, difWins, numOfPrecincts;
        double demVote, repVote, diffPercen;
        double diffTotal=0.0;

        for (District d: data.getDistricts(stateID)) {
            demWins = repWins = 0;
            precinctList = data.getPrecincts(stateID,d.getDistrictId());
            numOfPrecincts = precinctList.size();
            for (Precinct p: precinctList){
                demVote = data.getDemocrateVote(p.getPrecinctId());
                repVote = data.getRepublicanVote(p.getPrecinctId());
                if(demVote>repVote) {demWins++;}
                else if (repVote >= demVote){repWins++;}
            }
            difWins = Math.abs(demWins-repWins);
            diffPercen = ((double) difWins/ (double) numOfPrecincts)*100;
            diffTotal += diffPercen;
        }
        fairness = 100 - (diffTotal/numOfDistricts);
        return fairness;
    }

    private synchronized static double politicalFairness(StateID stateID){
        double fairness = 0;
        DataSingleton data = DataSingleton.getInstance();
        int numOfDistricts = data.getDistricts(stateID).size();
        List<Precinct> precinctList;
        int demWins, repWins, difWins, numOfPrecincts;
        double demVote, repVote, diffPercen;
        double diffTotal=0.0;

        for (District d: data.getDistricts(stateID)) {
            demWins = repWins = 0;
            precinctList = data.getPrecincts(stateID,d.getDistrictId());
            numOfPrecincts = precinctList.size();
            for (Precinct p: precinctList){
                demVote = data.getDemocrateVote(p.getPrecinctId());
                repVote = data.getRepublicanVote(p.getPrecinctId());
                if(demVote>repVote) {demWins++;}
                else if (repVote >= demVote){repWins++;}
            }
            difWins = Math.abs(demWins-repWins);
            diffPercen = ((double) difWins/ (double) numOfPrecincts)*100;
            diffTotal += diffPercen;
        }
        fairness = 100 - (diffTotal/numOfDistricts);
        return fairness;
    }

    // Calculation for the generated State
    public synchronized static double calculateFairnessGeneratedDistrict(List<District>districtList, HashMap<Integer, List<Precinct>> precinctData){
        double populationFair = populationFairnessGeneratedState(districtList,precinctData);
        double compactFair = compactnessFairnessGeneratedState(districtList,precinctData);
        double policalFair = politicalFairnessGeneratedState(districtList,precinctData);
        return ((populationFair+compactFair+policalFair)/3);
    }

    public synchronized static double calculateFairnessGeneratedDistrict(List<District>districtList, HashMap<Integer, List<Precinct>> precinctData,Integer populationPerc, Integer compactnessPerc, Integer politicalPrec){
        double populationFair = (populationFairnessGeneratedState(districtList,precinctData)) * (((double) populationPerc) / 100.0);
        double compactFair = (compactnessFairnessGeneratedState(districtList,precinctData)) * (((double) compactnessPerc) / 100.0);
        double policalFair = (politicalFairnessGeneratedState(districtList,precinctData)) * (((double) politicalPrec) / 100.0);
        return ((populationFair+compactFair+policalFair)/3.0);
    }

    public synchronized static double compactnessFairnessGeneratedState(List<District>districtList, HashMap<Integer, List<Precinct>> precinctData){
        double fairness = 0;
        int numOfDistricts = districtList.size();
        List<Precinct> precinctList;
        int demWins, repWins, difWins, numOfPrecincts;
        double demVote, repVote, diffPercen;
        double diffTotal=0.0;
        DataSingleton data = DataSingleton.getInstance();

        for (District d: districtList) {
            demWins = repWins = 0;
            precinctList = precinctData.get(d.getDistrictId());
            numOfPrecincts = precinctList.size();
            for (Precinct p: precinctList){
                demVote = data.getDemocrateVote(p.getPrecinctId());
                repVote = data.getRepublicanVote(p.getPrecinctId());
                if(demVote>repVote) {demWins++;}
                else if (repVote >= demVote){repWins++;}
            }
            difWins = Math.abs(demWins-repWins);
            diffPercen = ((double) difWins/ (double) numOfPrecincts)*100;
            diffTotal += diffPercen;
        }
        fairness = 100 - (diffTotal/numOfDistricts);
        return fairness;
    }

    public synchronized static double politicalFairnessGeneratedState(List<District>districtList, HashMap<Integer, List<Precinct>> precinctData){
        double fairness = 0;
        int numOfDistricts = districtList.size();
        List<Precinct> precinctList;
        int demWins, repWins, difWins, numOfPrecincts;
        double demVote, repVote, diffPercen;
        double diffTotal=0.0;
        DataSingleton data = DataSingleton.getInstance();

        for (District d: districtList) {
            demWins = repWins = 0;
            precinctList = precinctData.get(d.getDistrictId());
            numOfPrecincts = precinctList.size();
            for (Precinct p: precinctList){
                demVote = data.getDemocrateVote(p.getPrecinctId());
                repVote = data.getRepublicanVote(p.getPrecinctId());
                if(demVote>repVote) {demWins++;}
                else if (repVote >= demVote){repWins++;}
            }
            difWins = Math.abs(demWins-repWins);
            diffPercen = ((double) difWins/ (double) numOfPrecincts)*100;
            diffTotal += diffPercen;
        }
        fairness = 100 - (diffTotal/numOfDistricts);
        return fairness;
    }

    private synchronized static double populationFairnessGeneratedState(List<District>districtList, HashMap<Integer, List<Precinct>> precinctData){
        double fairness = 0.0;
        int numOfDistricts = districtList.size();
        double statePopulation = populationState(districtList, precinctData);
        double fairPopulation = statePopulation/numOfDistricts;
        double differenceTotal = 0.0;
        for (District d: districtList) {
            differenceTotal += Math.abs(fairPopulation - populationDistrict(d.getDistrictId(), precinctData));
        }
        double differenceAve = differenceTotal / numOfDistricts;
        fairness = 100 - ((differenceAve/fairPopulation)*100);
        return fairness;
    }

    private synchronized static double populationState(List<District>districtList, HashMap<Integer, List<Precinct>> precinctData){
        double population = 0.0;
        for (District d: districtList) {
            population += populationDistrict(d.getDistrictId(), precinctData);
        }
        return population;
    }

    private   synchronized static double populationDistrict(Integer districtID, HashMap<Integer, List<Precinct>> precinctData){
        double population = 0;
        DataSingleton ds = DataSingleton.getInstance();
        for (Precinct p: precinctData.get(districtID)) {
            population += ds.getPopulation(p.getPrecinctId()).getPopulation();
        }
        return population;
    }

    public static void main(String [] args){
        DataSingleton ds = DataSingleton.getInstance();
        ds.makeSureDataExist(StateID.NE);
        System.out.println("Population Fairness: "+ObjectiveFunction.populationFairness(StateID.NE));
        System.out.println("Political Fairness: "+ObjectiveFunction.politicalFairness(StateID.NE));
        System.out.println("Compactness Fairness: "+ObjectiveFunction.compactnessFairness(StateID.NE));
        System.out.println("Total State Fainess: " + ObjectiveFunction.databaseStateFairness(StateID.NE));
    }
}
