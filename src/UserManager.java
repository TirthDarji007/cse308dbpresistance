import gerrymandering.HibernateManager;
import javassist.NotFoundException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tirth
 */
public class UserManager {
    // (Key, Value) = (Userid, Admin)
    private static HashMap<Integer, Integer> loggedUsers = new HashMap<>();
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://mysql4.cs.stonybrook.edu/dolphins";
    static final String USER = "dolphins";
    static final String PASS = "changeit";

    /**
     * Login user to system.
     * @param email string
     * @param password string
     * @return userid on success and null on error
     */
    public synchronized static Integer login(String email, String password){
        try {
            HibernateManager hb = HibernateManager.getInstance();
            Map<String,Object> criteria = new HashMap<>();
            criteria.put("email",email);
            criteria.put("password", password);
            List<Object> list = hb.getRecordsBasedOnCriteria(User.class,criteria);
            if (list.size()<= 0){
                throw new NotFoundException("Database query return " + list.size() + " users");
            }
            User user = (User) list.get(0);
            loggedUsers.put(user.getUserId(),user.isAdmin());
            return user.getUserId();
        } catch (Exception ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error in login user: " + email);
            return null;
        } catch (Throwable ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error in login user: " + email);
            return null;
        }
    }

    /**
     *  Register New User.
     * @param email string
     * @param password string
     * @param isAdmin 0 or 1
     * @return
     */
    public synchronized static Integer register(String email, String password, Integer isAdmin){
        try {
            HibernateManager hb = HibernateManager.getInstance();
            User user = new User(email,password,isAdmin);
            List<Object> list = hb.getAllRecords(User.class);
            user.setUserId(list.size()+1);
            boolean result= false;
            //result = hb.persistToDB(user);
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection= DriverManager.getConnection(DB_URL,USER,PASS);
            connection.setAutoCommit(false);
            Statement statement= connection.createStatement();
            String SQL = "INSERT INTO users " + "VALUES (" + user.getUserId() + ",'" + user.getEmail() + "','" +
                    user.getPassword() + "'," + user.isAdmin() + ")";
            System.out.println(SQL);
            statement.executeUpdate(SQL);
            connection.commit();
            System.out.println("Registered user " + email + " successfully.");
            return login(email,password);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error in Registering user " + email);
            return null;
        }
        catch (Throwable throwable) {
            throwable.printStackTrace();
            System.out.println("Error in Registering user " + email);
            return null;
        }

    }

    /**
     * Logout the user
     * @param userid int
     * @return useid
     */
    public synchronized static Integer logout(Integer userid){
        if (loggedUsers.containsKey(userid)){
            loggedUsers.remove(userid);
        }else{
            System.out.println("Loggin out user " + userid + " that has not been logged in.");
        }
        return userid;
    }

    /**
     * Is the user logged in
     * @param userid
     * @return true if user is logged in.
     */
    public synchronized static boolean isLoggedIn(Integer userid){
        return loggedUsers.containsKey(userid);
    }

    public static void main(String [] args){
        //int id = login("tirth.darji@stonybrook.edu","test");
        int id = register("abc@awesome.com","test",0);
        System.out.println("user " + id + " logged in.");
    }
}
