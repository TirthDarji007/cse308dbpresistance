import org.json.JSONException;
import org.json.JSONObject;

public class Move {

    private int precinctID;
    private int fromDistrictID;
    private int toDistrictID;

    public Move(int precinctID, int fromDistrictID, int toDistrictID) {
        this.precinctID = precinctID;
        this.fromDistrictID = fromDistrictID;
        this.toDistrictID = toDistrictID;
    }

    public int getPrecinctID() {
        return precinctID;
    }

    public void setPrecinctID(int precinctID) {
        this.precinctID = precinctID;
    }

    public int getFromDistrictID() {
        return fromDistrictID;
    }

    public void setFromDistrictID(int fromDistrictID) {
        this.fromDistrictID = fromDistrictID;
    }

    public int getToDistrictID() {
        return toDistrictID;
    }

    public void setToDistrictID(int toDistrictID) {
        this.toDistrictID = toDistrictID;
    }

    @Override
    public String toString() {
        try {
            JSONObject jo = new JSONObject();
            jo.put("type", "Move");
            jo.put("precinctID", precinctID);
            jo.put("fromDistrictID", fromDistrictID);
            jo.put("toDistrictID", toDistrictID);
            return jo.toString();
        }catch (JSONException e){
            //e.printStackTrace();
            return "{" +
                    "\"type\":\"Move\","+
                    "\"precinctID\":\"" + precinctID + "\""+
                    ",\"fromDistrictID\":\"" + fromDistrictID +"\""+
                    ",\"toDistrictID\":\"" + toDistrictID + "\""+
                    '}';
        }
    }

    public static void main(String [] args){
        Move move = new Move(1,3,2);
        System.out.println(move.toString());
    }
}
