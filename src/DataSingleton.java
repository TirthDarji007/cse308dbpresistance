import gerrymandering.HibernateManager;
import gerrymandering.model.*;
import javassist.NotFoundException;

import java.util.*;

public class DataSingleton {
    private static DataSingleton ourInstance = new DataSingleton();

    private static final String teamName = "Dolphins";

    public static DataSingleton getInstance() {
        return ourInstance;
    }

    // Instance Variable to store data
    private static HashMap<StateID, State> stateData;
    private static HashMap<StateID, List<District>> districtData;
    //This integer is the (stateId_enum_number_value) + (district_id)
    private static HashMap<Integer, List<Precinct>> precinctData;
    private static HashMap<Integer, Population> populationData;
    private static HashMap<Integer, List<ElectionData>> electionData;
    private static HashMap<Integer, Double> republicanVoteData;
    private static HashMap<Integer, Double> democrateVoteData;

    private DataSingleton() {
        stateData = new HashMap<>();
        districtData = new HashMap<>();
        precinctData = new HashMap<>();
        precinctData = new HashMap<>();
        electionData = new HashMap<>();
        populationData = new HashMap<>();
        republicanVoteData = new HashMap<>();
        democrateVoteData = new HashMap<>();
    }

    /**
     * This method goes to the main database and downloads state objects, districts and precincts.
     *
     * @param stateID
     */
    private void downloadData(StateID stateID){
        try {
            // Local Variables
            HibernateManager hb = HibernateManager.getInstance();
            Iterator<Object> itrDist, itrPrecinct, itrPopulation, itrElectionData, itrPopDol;
            List<Object> listPrec, listPopulation, listElectionData, listPopDol;
            List<District> distrcits = new LinkedList<>();
            List<Precinct> precincts = new LinkedList<>();
            List<ElectionData> elections = new LinkedList<>();
            District d;
            Precinct p;
            Population population;
            ElectionData e;
            Integer precinct_id;
            PopulationDolphin pd;

            // Download the states
            Map<String,Object> criteria = new HashMap<>();
            criteria.put("shortName",stateID.name());
            List<Object> list = hb.getRecordsBasedOnCriteria(State.class,criteria);
            if (list.size()<= 0){
                throw new NotFoundException("Database query return " + list.size() + " states");
            }
            State state = (State) list.get(0);
            stateData.put(stateID,state);

            // download districts of the states.
            criteria.clear();
            criteria.put("stateId",state.getStateId());
            list = hb.getRecordsBasedOnCriteria(District.class,criteria);
            itrDist = list.iterator();
            while(itrDist.hasNext()){
                d = (District) itrDist.next();
                distrcits.add(d);

                // get the precincts of a district
                criteria.clear();
                precincts = new LinkedList<>();
                criteria.put("districtId",d.getDistrictId());
                listPrec = hb.getRecordsBasedOnCriteria(Precinct.class,criteria);
                itrPrecinct = listPrec.iterator();
                while (itrPrecinct.hasNext()){
                    p = (Precinct) itrPrecinct.next();
                    precincts.add(p);
                }

                //get population
                //error in gerrymandering_db_presistence.jar. Following query will always fail.
                /*
                criteria.clear();
                criteria.put("districtId",d.getDistrictId());
                listPopulation = hb.getRecordsBasedOnCriteria(Population.class,criteria);
                itrPopulation = listPopulation.iterator();
                while (itrPopulation.hasNext()){
                    population = (Population) itrPopulation.next();
                    populationData.put(population.getPrecinctId(),population);
                }
                 */
                criteria.clear();
                criteria.put("districtId",d.getDistrictId());
                listPopDol = hb.getRecordsBasedOnCriteria(PopulationDolphin.class,criteria);
                itrPopDol = listPopDol.iterator();
                while (itrPopDol.hasNext()){
                    pd = (PopulationDolphin) itrPopDol.next();
                    population = new Population(teamName);
                    population.setPoputationId(pd.getPoputationId());
                    population.setDate(pd.getDate());
                    population.setDistrictId(pd.getDistrictId());
                    population.setPopulation(pd.getPopulation());
                    population.setPrecinctId(pd.getPrecinctId());
                    populationData.put(population.getPrecinctId(),population);
                }


                //Load election data using district id
                criteria.clear();
                criteria.put("districtId",d.getDistrictId());
                listElectionData = hb.getRecordsBasedOnCriteria(ElectionData.class,criteria);
                itrElectionData = listElectionData.iterator();
                while(itrElectionData.hasNext()){
                    e = (ElectionData) itrElectionData.next();
                    elections = electionData.get(e.getPrecinctId());
                    if (elections == null){
                        //create new list and add to data
                        elections = new LinkedList<>();
                        elections.add(e);
                        electionData.put(e.getPrecinctId(),elections);
                    }else{
                        electionData.get(e.getPrecinctId()).add(e);
                    }
                    //save vote data in hashmap
                    switch (e.getPartyName()){
                        case Republican:
                            republicanVoteData.put(e.getPrecinctId(),e.getVoteCount());
                            break;
                        case Democratic:
                            democrateVoteData.put(e.getPrecinctId(),e.getVoteCount());
                            break;
                        default:
                            break;
                    }
                }

                precinctData.put(stateID.ordinal() + d.getDistrictId(),precincts);
            }
            districtData.put(stateID,distrcits);

        }
        catch (Exception e) {
            System.out.println("Error in downloading the data");
            e.printStackTrace();
        }
        catch (Throwable t){
            System.out.println("Error in downloading the data");
            t.printStackTrace();
        }
    }

    /**
     * This method checks if we have the data stored.
     * If not stored then downloads from the database.
     * @param stateID
     */
    public void makeSureDataExist(StateID stateID){
        if (! stateData.keySet().contains(stateID)){
            downloadData(stateID);
        }
    }

    /**
     * Given the stateID it returns the state object from the database.
     * @param stateID
     * @return State
     */
    public State getState(StateID stateID){
        makeSureDataExist(stateID);
        return stateData.get(stateID);
    }

    /**
     * Given the stateID this will return the list of district.
     * @param stateID
     * @return List<District>
     */
    public List<District> getDistricts(StateID stateID){
        makeSureDataExist(stateID);
        return districtData.get(stateID);
    }

    /**
     * Given the stateId and districtID,
     * ir will return the precinct contained in the district of the state.
     * @param stateID
     * @param districtId
     * @return List<Precinct>
     */
    public List<Precinct> getPrecincts(StateID stateID, Integer districtId){
        makeSureDataExist(stateID);
        return precinctData.get(stateID.ordinal() + districtId);
    }

    /**
     * Get precint object by precinct id
     * @param stateID
     * @param precinctId
     * @return
     */
    public Precinct getPrecinct(StateID stateID,Integer precinctId){
        makeSureDataExist(stateID);
        for (District d: getDistricts(stateID)){
            for (Precinct p: getPrecincts(stateID,d.getDistrictId())){
                if (p.getPrecinctId() == precinctId){
                    return  p;
                }
            }
        }
        return null;
    }

    /**
     * Given the stateID, it will return the list of precincts inside the state.
     * @param stateID
     * @return
     */
    public List<Precinct> getPrecinsts(StateID stateID){
        makeSureDataExist(stateID);
        List<Precinct> record = new LinkedList<>();
        List<Precinct> tempPrec;
        Iterator<Precinct> itrPrec;
        List<District> dislist = getDistricts(stateID);
        Iterator<District> itr = dislist.iterator();
        while(itr.hasNext()){
            tempPrec = getPrecincts(stateID, itr.next().getDistrictId());
            itrPrec = tempPrec.iterator();
            while (itrPrec.hasNext()){
                record.add(itrPrec.next());
            }
        }
        return record;
    }

    /**
     * This method returns the Population object by the precinctID.
     * @param precinctID
     * @return Population
     */
    public Population getPopulation(Integer precinctID){
        return populationData.get(precinctID);
    }


    /**
     * Get poulation object list from the stateid and districtid
     * @param stateID
     * @param districtId
     * @return
     */
    public List<Population> getPopulation(StateID stateID, Integer districtId){
        makeSureDataExist(stateID);
        List<Population> populationList = new LinkedList<>();
        for (Precinct p: precinctData.get(stateID.ordinal()+districtId) ) {
            populationList.add(0,getPopulation(p.getPrecinctId()));
        }
        return populationList;
    }

    /**
     * This method returns the list of election data from given precinctID
     * @param precinctID
     * @return List of election data of precinct.
     */
    public List<ElectionData> getElectionData(Integer precinctID){
        return electionData.get(precinctID);
    }


    /**
     * This method returns the total population of the given district.
     * @param stateId
     * @param districtID
     * @return
     */
    public  double populationDistrict(StateID stateId, Integer districtID){
        makeSureDataExist(stateId);
        double population = 0;
        for (Population p: this.getPopulation(stateId,districtID)) {
            population += p.getPopulation();
        }
        return population;
    }

    /**
     * This method returns the population of given state.
     * @param stateID
     * @return
     */
    public double populationState(StateID stateID){
        makeSureDataExist(stateID);
        double population = 0.0;
        for (District d: this.getDistricts(stateID)) {
            population += this.populationDistrict(stateID,d.getDistrictId());
        }
        return population;
    }

    /**
     * Get the republican vote for precinct.
     * @param precinctId
     * @return
     */
    public double getRepublicanVote(Integer precinctId){
        return republicanVoteData.get(precinctId);
    }

    /**
     * Get democrate vote for precinct.
     * @param precinctId
     * @return
     */
    public double getDemocrateVote(Integer precinctId){
        return democrateVoteData.get(precinctId);
    }

    public static void main(String [] args){
        DataSingleton ds = DataSingleton.getInstance();
        List<Precinct> pl = ds.getPrecinsts(StateID.NE);
        Iterator<Precinct> itr = pl.iterator();
        Precinct p;
        while (itr.hasNext()){
            p = itr.next();
            System.out.println(p.getPrecinctId() + " " + p.getDistrictId() +  " " + p.getBoundaryJSON());
        }
    }
}
