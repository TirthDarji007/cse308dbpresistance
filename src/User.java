//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "USERS"
)
public class User implements Serializable {
    @Id
    @GeneratedValue
    @Column(
            name = "USERID"
    )
    private Integer userId;

    @Column(
            name = "EMAIL"
    )
    private String email;

    @Column(
            name = "PASSWORD"
    )
    private String password;

    @Column(
            name = "ADMIN"
    )
    private Integer isAdmin;

    public User(){

    }

    public User(String email, String password, Integer isAdmin) {
        this.email = email;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer isAdmin() {
        return isAdmin;
    }

    public void setAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }
}
