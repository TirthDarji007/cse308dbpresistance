import gerrymandering.model.District;
import gerrymandering.model.Precinct;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.geojson.GeoJsonReader;

import javax.websocket.Session;
import java.io.IOException;
import java.util.*;

public class RegionGrowing implements Runnable {
    private StateID stateId;
    private GeneratedState generatedState;
    private DataSingleton dataSingleton;
    private Random random;
    private HashMap<Integer, Precinct> remainPrecincts; //Remain Precincts to add. key is pricinctID
    //private HashMap<Integer, Precinct> allPrecincts;

    // Default values
    private int NUM_OF_BOUNDARY_PRECINCTS = 10;
    private final int UNASSIGNED_DISTRICT_ID = -1;
    private final double MAX_OBECTIVE_TOLARANCE = 3.0;
    private final int MAX_MOVES = 5000;
    private double previousFairness;
    private double currentFairness;
    private volatile int moveCount = 0;
    private Integer populationPerc = 100;
    private Integer compactnessPerc = 100;
    private Integer politicalPrec = 100;
    private int numberOfDistricts;
    private boolean changeDistrictNumbers = false;
    private boolean useBestPrecinct = true;
    private List<Integer> seedPrecinct = new LinkedList<>();
    private static final String TEAM_NAME = "Dolphins";

    //Thread Variables
    private volatile List<String> moves;
    private volatile List<Move> moveList;
    private volatile boolean running = true;
    private volatile boolean paused = false;
    private final Object pauseLock = new Object();

    // Websocket session
    private Session session;



    /**
     * Constructor
     *
     * @param stateID
     * @param populationPerc
     * @param compactnessPerc
     * @param politicalPrec
     * @param session
     */
    public RegionGrowing(
            StateID stateID,
            Integer populationPerc,
            Integer compactnessPerc,
            Integer politicalPrec,
            Session session,
            boolean changeDistrictNumbers,
            int newDistrictNumber,
            List<Integer> seedPrecinctId) {
        this.stateId = stateID;
        this.generatedState = new GeneratedState(stateID);
        this.dataSingleton = DataSingleton.getInstance();
        this.random = new Random();
        this.remainPrecincts = new HashMap<>();
        //this.allPrecincts = new HashMap<>();
        this.moves = new LinkedList<>();
        this.moveList = new LinkedList<>();
        this.populationPerc = populationPerc;
        this.compactnessPerc = compactnessPerc;
        this.populationPerc = populationPerc;
        this.session = session;
        this.changeDistrictNumbers = changeDistrictNumbers;
        this.numberOfDistricts = newDistrictNumber;
        this.seedPrecinct = seedPrecinctId;
        this.setupRG();
    }

    //Deprecicated.
    public RegionGrowing(
            StateID stateID,
            Integer populationPerc,
            Integer compactnessPerc,
            Integer politicalPrec,
            Session session) {
        this.stateId = stateID;
        this.generatedState = new GeneratedState(stateID);
        this.dataSingleton = DataSingleton.getInstance();
        this.random = new Random();
        this.remainPrecincts = new HashMap<>();
        //this.allPrecincts = new HashMap<>();
        this.moves = new LinkedList<>();
        this.moveList = new LinkedList<>();
        this.populationPerc = populationPerc;
        this.compactnessPerc = compactnessPerc;
        this.populationPerc = populationPerc;
        this.session = session;
        this.setupRG();
    }


    /**
     * Depreciated. Only for testing purpose.
     *
     * @param stateID
     */
    public RegionGrowing(StateID stateID) {
        this.stateId = stateID;
        this.generatedState = new GeneratedState(stateID);
        this.dataSingleton = DataSingleton.getInstance();
        this.random = new Random();
        this.remainPrecincts = new HashMap<>();
        //this.allPrecincts = new HashMap<>();
        this.moves = new LinkedList<>();
        this.moveList = new LinkedList<>();
        this.setupRG();
    }


    /**
     * Depreciated. Only for testing purpose.
     *
     * @param stateID
     * @param populationPerc
     * @param compactnessPerc
     * @param politicalPrec
     */
    public RegionGrowing(StateID stateID, Integer populationPerc, Integer compactnessPerc, Integer politicalPrec) {
        this.stateId = stateID;
        this.generatedState = new GeneratedState(stateID);
        this.dataSingleton = DataSingleton.getInstance();
        this.random = new Random();
        this.remainPrecincts = new HashMap<>();
        this.remainPrecincts = new HashMap<>();
        this.moves = new LinkedList<>();
        this.populationPerc = populationPerc;
        this.compactnessPerc = compactnessPerc;
        this.populationPerc = populationPerc;
        this.moveList = new LinkedList<>();
        this.setupRG();
    }

    public List<String> getMoves() {
        return moves;
    }

    public String getMovesJson() {
        return Utilities.moveJsonList(moves);
    }

    public int getNumberOfDistricts() {
        return numberOfDistricts;
    }

    public void setNumberOfDistricts(int numberOfDistricts) {
        this.numberOfDistricts = numberOfDistricts;
        this.changeDistrictNumbers = true;
    }

    public boolean isChangeDistrictNumbers() {
        return changeDistrictNumbers;
    }

    public void useFirstPrecinctAlgo() {this.useBestPrecinct = false;}

    @Override
    public void run() {
        Precinct selectedPrecinct;
        District selectedDistrict;
        Move move;
        double newFairness;
        for (Move m: moveList){
            sendMove(m);
        }

        while (running) {
            synchronized (pauseLock) {
                if (!running) {
                    break;
                }
                if (paused) {
                    try {
                        pauseLock.wait();
                    }
                    catch (InterruptedException ex) {
                        break;
                    }
                    if (!running) {
                        break;
                    }
                }
            }


            if (remainPrecincts.size() <= 0) {
                // Stop Thread, we are finished with all precincts.
                newFairness = ObjectiveFunction.
                        calculateFairnessGeneratedDistrict(this.generatedState.getDistrictList(),
                                this.generatedState.getPrecinctData(),
                                populationPerc, compactnessPerc, politicalPrec);
                //System.out.println("Old Fairness: " + ObjectiveFunction.databaseStateFairness(stateId) +
                //        " New Fairness: " + newFairness);
                System.out.println(" New Fairness: " + newFairness);
                this.running = false;
            } else {
                selectedDistrict = selectDistrict();
                if (useBestPrecinct) {
                    selectedPrecinct = selectBestPrecinct(selectedDistrict);
                } else {
                    selectedPrecinct = selectFirstPrecinct(selectedDistrict);
                }

                if(selectedPrecinct == null){
                    //System.out.println("RG: selected precinct is null. Possibility of district covered");
                    continue;
                }

                // Make direct move since selectBestPrecinct returns the best precinct that can be added.
                System.out.println("CurrentFairness: " + currentFairness + " PreviousFairness: " + previousFairness);
                if (!(moveCount <= MAX_MOVES) || (this.currentFairness > this.previousFairness) ||
                        (Math.abs(this.currentFairness - this.previousFairness) < MAX_OBECTIVE_TOLARANCE)) {
                    move = makeMove(selectedPrecinct, selectedPrecinct.getDistrictId(), selectedDistrict.getDistrictId());
                    moveCount++;
                    System.out.println(moveCount + " Move Made: " + move.toString());
                    this.moves.add(move.toString());
                    this.previousFairness = this.currentFairness;
                    //Send move to front end
                    sendMove(move);
                } else {
                    // Reject the move.
                    System.out.println(moveCount + " move rejected " +
                            new Move(selectedPrecinct.getPrecinctId(), selectedPrecinct.getDistrictId(),
                                    selectedDistrict.getDistrictId()).toString());
                    moveCount++;
                }
            }
        }

    }

    private void sendMove(Move move) {
        try {
            session.getBasicRemote().sendText(move.toString());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates same number of district with random precinct inside each district.
     */
    private void setupRG() {
        List<Precinct> precinctsDist, precinctList;
        List<District> districtList;
        Precinct selectedp;
        int rand;
        Precinct pcopy;
        District district;
        Move move;
        this.dataSingleton.makeSureDataExist(this.stateId);
        //this.numberOfDistricts = this.dataSingleton.getDistricts(this.stateId).size();
        if (!this.changeDistrictNumbers) {
            System.out.println("RG has same number of districts.");
            for (District d : dataSingleton.getDistricts(stateId)) {
                generatedState.addDistrict(d);
                precinctsDist = dataSingleton.getPrecincts(stateId, d.getDistrictId());
                rand = random.nextInt(precinctsDist.size());
                for (Precinct p : precinctsDist) {
                    pcopy = this.generatedState.copyPrecinct(p);
                    pcopy.setDistrictId(UNASSIGNED_DISTRICT_ID);
                    this.remainPrecincts.put(p.getPrecinctId(), pcopy);
                }
                selectedp = precinctsDist.get(rand);
                generatedState.addPrecinct(d.getDistrictId(), selectedp);
                move = new Move(selectedp.getPrecinctId(),this.UNASSIGNED_DISTRICT_ID,d.getDistrictId());
                this.remainPrecincts.remove(selectedp.getPrecinctId());
                //sendMove(move);
                moveList.add(move);
                moveCount++;
            }
        } else {
            // User has changed the number of districts.
            try {
                districtList = this.dataSingleton.getDistricts(stateId);
                precinctList = this.dataSingleton.getPrecinsts(stateId);
                // crated requested number of districts
                for (int i = 0; i < this.numberOfDistricts; i++) {
                    if (i < districtList.size()) {
                        district = districtList.get(i);
                    } else {
                        //create new district in state
                        District dtemp = new District(dataSingleton.getState(stateId).getStateId(),
                                dataSingleton.getState(stateId).getName() + " new district " + i,
                                "{}", TEAM_NAME, "");
                        dtemp.setDistrictId((dataSingleton.getState(stateId).getStateId() * 2) + (i * i));
                        district = dtemp;
                    }
                    generatedState.addDistrict(district);
                }

                // add precincts to generated state & remove district belonging id
                for (Precinct p : precinctList) {
                    pcopy = this.generatedState.copyPrecinct(p);
                    pcopy.setDistrictId(UNASSIGNED_DISTRICT_ID);
                    this.remainPrecincts.put(p.getPrecinctId(), pcopy);
                }

                // add one precinct to each district
                /*
                for (int i = 0; i < numberOfDistricts; i++) {
                    rand = random.nextInt(precinctList.size());
                    selectedp = precinctList.get(rand);
                    generatedState.addPrecinct(generatedState.getDistrictList().get(i).getDistrictId(), selectedp);
                    move = new Move(selectedp.getPrecinctId(),-1, generatedState.getDistrictList().get(i).getDistrictId());
                    moveCount++;
                    moveList.add(move);
                    //sendMove(move);
                }*/
                for (int i = 0; i < numberOfDistricts; i++) {
                    //rand = random.nextInt(precinctList.size());
                    //selectedp = precinctList.get(rand);
                    selectedp = this.remainPrecincts.get(this.seedPrecinct.get(i));
                    generatedState.addPrecinct(generatedState.getDistrictList().get(i).getDistrictId(), selectedp);
                    move = new Move(selectedp.getPrecinctId(),this.UNASSIGNED_DISTRICT_ID, generatedState.getDistrictList().get(i).getDistrictId());
                    moveCount++;
                    moveList.add(move);
                    this.remainPrecincts.remove(this.seedPrecinct.get(i));
                    //sendMove(move);
                }

            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error while creating new district in setupRG");
            }


        }


        System.out.println(" RG has " + this.generatedState.getDistrictList().size() + " districts.");
        // calculate the generated state fairness.
        this.compactnessPerc = 0;
        this.previousFairness = ObjectiveFunction.calculateFairnessGeneratedDistrict(
                this.generatedState.getDistrictList(),
                this.generatedState.getPrecinctData(),
                populationPerc, compactnessPerc, politicalPrec);
    }

    /**
     * Selects the next district from where next precinct
     *
     * @return
     */
    private District selectDistrict() {
        int rand = random.nextInt(generatedState.getDistrictList().size());
        return generatedState.getDistrictList().get(rand);
    }

    /**
     * Select the next Precinct.
     *
     * @return
     */
    private Precinct selectBestPrecinct(District district) {
        Precinct precinct;
        Integer maxObjValPrecId;
        List<Precinct> precinctList;
        int rand;

        // select precinct of  district
        precinctList = generatedState.getPrecicntList(district.getDistrictId());
        rand = random.nextInt(precinctList.size());
        precinct = precinctList.get(rand);

        // Select the best boundary precinct.
        List<Integer> osortedByDistance = precinctsIDByDistance(precinct);
        List<Integer> sortedByDistance = new ArrayList<>();
        int index=0;
        int counter = NUM_OF_BOUNDARY_PRECINCTS;
        for (Integer pid: osortedByDistance){
            if (remainPrecincts.containsKey(pid)){
                // precinct is unassigned
                //System.out.println(index + " is added to sorted distance");
                sortedByDistance.add(pid);
                index++;
                break;
            }else{
                // precinct is assigned
                if (generatedState.getPrecinct(pid).getDistrictId() == district.getDistrictId() ){
                    // inside the same district
                    //System.out.println(index + " is skiped to sorted distance");
                    index++;
                    continue;

                }else{
                    // not inside the same district
                    //System.out.println(index + " is break to sorted distance");
                    counter--;
                    if(counter < 0) {
                        break;
                    }
                }
            }
        }
        if (sortedByDistance.size() == 0){
            return null;
        }


        // Find the best of all
        int num = (NUM_OF_BOUNDARY_PRECINCTS < sortedByDistance.size()) ? NUM_OF_BOUNDARY_PRECINCTS : sortedByDistance.size();
        maxObjValPrecId = remainPrecincts.get(sortedByDistance.get(0)).getPrecinctId();
        double maxObjectiveVal = temporaryMove(remainPrecincts.get(sortedByDistance.get(0)),
                this.remainPrecincts.get(maxObjValPrecId).getDistrictId(), district.getDistrictId());
        ;
        double currentObjVal;
        Integer currentPrecId;
        for (int i = 1; i < num; i++) {
            currentPrecId = remainPrecincts.get(sortedByDistance.get(i)).getPrecinctId();
            currentObjVal = temporaryMove(remainPrecincts.get(sortedByDistance.get(i)),
                    this.remainPrecincts.get(currentPrecId).getDistrictId(), district.getDistrictId());
            if (currentObjVal > maxObjectiveVal) {
                maxObjectiveVal = currentObjVal;
                maxObjValPrecId = currentPrecId;
            }
        }

        this.currentFairness = maxObjectiveVal;
        System.out.println("Best Selected Precinct Fairness: " + maxObjectiveVal);
        return this.remainPrecincts.get(maxObjValPrecId);

    }


    private Precinct selectFirstPrecinct(District district) {
        Precinct precinct;
        Integer maxObjValPrecId;
        List<Precinct> precinctList;
        int rand;

        // select precinct of  district
        precinctList = generatedState.getPrecicntList(district.getDistrictId());
        rand = random.nextInt(precinctList.size());
        precinct = precinctList.get(rand);

        // Select the first boundary precinct.
        List<Integer> sortedByDistance = precinctsIDByDistance(precinct);
        List<Integer> nsortedByDistance = new ArrayList<>();
        int index=0;
        for (Integer pid: sortedByDistance){
            if (remainPrecincts.containsKey(pid)){
                // precinct is unassigned
                //System.out.println(index + " is added to sorted distance");
                nsortedByDistance.add(pid);
                index++;
                break;
            }else{
                // precinct is assigned
                if (generatedState.getPrecinct(pid).getDistrictId() == district.getDistrictId() ){
                    // inside the same district
                    //System.out.println(index + " is skiped to sorted distance");
                    index++;
                    continue;

                }else{
                    // not inside the same district
                    //System.out.println(index + " is break to sorted distance");
                    break;
                }
            }
        }

        if (nsortedByDistance.size() ==0 ){
            //System.out.println("new sorted list by distance has no precinct.");
            return null;
        }

        /*
        maxObjValPrecId = remainPrecincts.get(sortedByDistance.get(0)).getPrecinctId();
        double maxObjectiveVal = temporaryMove(remainPrecincts.get(sortedByDistance.get(0)),
                this.remainPrecincts.get(maxObjValPrecId).getDistrictId(), district.getDistrictId());
        */
        maxObjValPrecId = remainPrecincts.get(nsortedByDistance.get(0)).getPrecinctId();
        double maxObjectiveVal = temporaryMove(remainPrecincts.get(nsortedByDistance.get(0)),
                this.remainPrecincts.get(maxObjValPrecId).getDistrictId(), district.getDistrictId());

        // Update the fairness value.
        this.currentFairness = maxObjectiveVal;
        System.out.println("First Selected Precinct Fairness: " + maxObjectiveVal);
        return this.remainPrecincts.get(maxObjValPrecId);
    }


    public List<Precinct> getNeighbors(Precinct boundaryPrecinct, Collection<Precinct> allPrecincts) {

        Geometry pbi = null, bpi = null;
        String pb;
        GeoJsonReader reader = new GeoJsonReader();
        pb = boundaryPrecinct.getBoundaryJSON();
        try {
            bpi = reader.read(pb);
        }
        catch (ParseException e1) {
            e1.printStackTrace();
        }
        List<Precinct> realNeighborList = new ArrayList<Precinct>();
        for (Precinct p : allPrecincts) {
            pb = p.getBoundaryJSON();
            try {
                pbi = reader.read(pb);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            if (pbi.intersects(bpi)) {
                realNeighborList.add(p);
            }
        }
        return realNeighborList;
    }

    /**
     * @param precinct
     * @return the list of precinctsID from closest to furthest from given precinct.
     */
    private List<Integer> precinctsIDByDistance(Precinct precinct) {
        HashMap<Integer, Double> distanceMap = new HashMap<>();
        //for (Precinct p : this.remainPrecincts.values()) {
        for (Precinct p : this.dataSingleton.getPrecinsts(stateId)) {
            distanceMap.put(p.getPrecinctId(), Utilities.distanceJSONPoint(precinct.getCenterPointJSON(),
                    p.getCenterPointJSON()));
        }
        HashMap<Integer, Double> sortedByDistance = Utilities.sortByValue(distanceMap);
        List<Integer> list = new ArrayList<Integer>(sortedByDistance.keySet());
        return list;
    }


    public Move makeMove(Precinct precinct, Integer fromDistrict, Integer toDistrict) {
        Move move = new Move(precinct.getPrecinctId(), fromDistrict, toDistrict);
        precinct.setDistrictId(toDistrict);
        this.generatedState.addPrecinct(toDistrict, precinct);
        this.remainPrecincts.remove(precinct.getPrecinctId());
        return move;
    }

    public Move revertMove(Move move) {
        Move reverseMove = new Move(move.getPrecinctID(), move.getToDistrictID(), move.getFromDistrictID());
        Precinct precinct = this.generatedState.getPrecinct(reverseMove.getPrecinctID());
        if (precinct == null) {
            System.out.println("reverMove couldn't find the precinct in generated state.");
        }
        precinct.setDistrictId(reverseMove.getToDistrictID());
        this.generatedState.removePrecinct(reverseMove.getFromDistrictID(), precinct);
        this.remainPrecincts.put(precinct.getPrecinctId(), precinct);
        return reverseMove;
    }

    public double temporaryMove(Precinct precinct, Integer fromDistrict, Integer toDistrict) {
        Move move = makeMove(precinct, fromDistrict, toDistrict);
        double fairness = ObjectiveFunction.calculateFairnessGeneratedDistrict(this.generatedState.getDistrictList(),
                this.generatedState.getPrecinctData(), populationPerc, compactnessPerc, politicalPrec);
        revertMove(move);
        return fairness;
    }

    public void finalizeMove(Precinct precinct, Integer fromDistrict, Integer toDistrict) {
        Move move = makeMove(precinct, fromDistrict, toDistrict);
        moveCount++;
        System.out.println(moveCount + " Move Made: " + move.toString());
        this.moves.add(move.toString());
        this.previousFairness = this.currentFairness;
        //Send move to front end
        sendMove(move);
    }

    // Resume the thread
    public void resume() {
        synchronized (pauseLock) {
            paused = false;
            pauseLock.notifyAll();
        }
    }

    // Stop processing this Thread
    public void stop() {
        running = false;
        resume();
    }

    // Pause current Thread.
    public void pause() {
        paused = true;
    }

}