import java.util.LinkedList;
import java.util.List;

public class TestThread implements Runnable {
    private List<Integer> list;
    private volatile boolean running = true;
    private volatile boolean paused = false;
    private final Object pauseLock = new Object();

    public TestThread(){
        this.list = new LinkedList<>();
    }

    public List<Integer> getList() {
        return list;
    }

    public void finish(){
        this.running = false;
    }

    @Override
    public void run() {
        int count = 0;
        while (running){
            synchronized (pauseLock) {
                if (!running) { // may have changed while waiting to
                    // synchronize on pauseLock
                    break;
                }
                if (paused) {
                    try {
                        pauseLock.wait(); // will cause this Thread to block until
                        // another thread calls pauseLock.notifyAll()
                        // Note that calling wait() will
                        // relinquish the synchronized lock that this
                        // thread holds on pauseLock so another thread
                        // can acquire the lock to call notifyAll()
                        // (link with explanation below this code)
                    } catch (InterruptedException ex) {
                        break;
                    }
                    if (!running) { // running might have changed since we paused
                        break;
                    }
                }
            }
            list.add(count);
            System.out.println("count from run: "+count);
            count++;
            try {
                Thread.sleep(200);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void resume() {
        synchronized (pauseLock) {
            paused = false;
            pauseLock.notifyAll(); // Unblocks thread
        }
    }

    public void stop() {
        running = false;
        // you might also want to interrupt() the Thread that is
        // running this Runnable, too, or perhaps call:
        resume();
        // to unblock
    }

    public void pause() {
        // you may want to throw an IllegalStateException if !running
        paused = true;
    }

    public static void main(String [] args){
        List<Integer> list;
        int count = 0;
        TestThread test = new TestThread();
        Thread mythread = new Thread(test);
        mythread.start();
        while (true) {
            try {
                count++;
                if(count == 8){
                    test.stop();
                    break;
                }
                Thread.sleep(2000);
                test.pause();
                list = test.getList();
                System.out.println("List from Main =" + list);
                test.resume();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        System.out.println("Outsidde the loop");
        System.out.println("outside List val " + test.getList());

    }
}
