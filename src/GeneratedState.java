import gerrymandering.model.District;
import gerrymandering.model.Precinct;
import gerrymandering.model.State;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GeneratedState implements Serializable {
    private static final String teamName = "Dolphins";
    private State state;
    private StateID stateID;
    private List<District> districtList;
    // Precinct list by districtID
    private HashMap<Integer, List<Precinct>> precinctData;

    public GeneratedState(StateID stateID){
        this.stateID = stateID;
        this.state = copyState(DataSingleton.getInstance().getState(stateID));
        this.districtList = new LinkedList<>();
        this.precinctData = new HashMap<>();
    }

    public State getState() {
        return state;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public List<Precinct> getPrecicntList(Integer districtID){
        return precinctData.get(districtID);
    }

    public HashMap<Integer, List<Precinct>> getPrecinctData() {
        return precinctData;
    }

    public void setStateID(StateID stateID) {
        this.stateID = stateID;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }

    public void setPrecinctData(HashMap<Integer, List<Precinct>> precinctData) {
        this.precinctData = precinctData;
    }

    public Precinct getPrecinct(Integer precinctID){
        for (List<Precinct> precinctList: this.precinctData.values()){
            for(Precinct p: precinctList){
                if (p.getPrecinctId() == precinctID){
                    return p;
                }
            }
        }
        return null;
    }

    public List<Precinct> getPrecinctList(){
        List<Precinct> precinctList = new LinkedList<>();
        for (District d: this.getDistrictList()){
            precinctList.addAll(this.getPrecicntList(d.getDistrictId()));
        }
        return precinctList;
    }

    public void addDistrict(District district){
        this.districtList.add(copyDistrict(district));
    }

    public void setState(State state){
        this.state = copyState(state);
    }

    public void addPrecinct(Integer districtID, Precinct precinct){
        List<Precinct> precinctList = this.precinctData.get(districtID);
        if (precinctList != null){
            precinctList.add(copyPrecinct(precinct));
        }
        else{
            precinctList = new LinkedList<>();
            precinctList.add(copyPrecinct(precinct));
            this.precinctData.put(districtID,precinctList);
        }
    }



    public void removePrecinct(Integer districtID, Precinct precinct){
        List<Precinct> precinctList = this.precinctData.get(districtID);
        int index = -1, loc = -1;
        if (precinctList != null){
            for (Precinct p: precinctList){
                index++;
                if(p.getPrecinctId() == precinct.getPrecinctId()){
                    loc = index;
                    break;
                }
            }
            if(loc != -1){
                precinctList.remove(loc);
            }
        }
    }

    public State copyState(State state){
        State ns = new State(teamName);
        ns.setName(state.getName());
        ns.setShortName(state.getShortName());
        ns.setStateId(state.getStateId());
        ns.setConstitutionText(state.getConstitutionText());
        return ns;
    }

    public District copyDistrict(District district){
        District nd = new District(teamName);
        nd.setDistrictId(district.getDistrictId());
        nd.setName(district.getName());
        try {
            nd.setBoundary(district.getBoundary());
            nd.setStateId(district.getStateId());
        }catch (Exception e){
            e.printStackTrace();
        }
        return nd;
    }

    public Precinct copyPrecinct(Precinct precinct){
        Precinct np = new Precinct(teamName);
        np.setPrecinctId(precinct.getPrecinctId());
        np.setDistrictId(precinct.getDistrictId());
        try {
            np.setBoundary(precinct.getBoundaryJSON());
            np.setCenterPointJSON(precinct.getCenterPointJSON());
        }catch (Exception e ){
            e.printStackTrace();
        }
        return np;
    }

    /**
     * For simulated Annealing
     * Make sure you have created generated state Object.
     */
    public void copyAllData(){
        if (this.state != null){
            DataSingleton ds = DataSingleton.getInstance();
            List<Precinct> precinctList;
            for (District d: ds.getDistricts(this.stateID)){
                precinctList = new LinkedList<>();
                this.districtList.add(copyDistrict(d));
                for (Precinct p: ds.getPrecincts(this.stateID,d.getDistrictId())){
                    precinctList.add(copyPrecinct(p));
                }
                this.precinctData.put(d.getDistrictId(),precinctList);
            }
        }
    }

    public double populationState(){
        double population = 0.0;
        for (District d: this.districtList) {
            population += this.populationDistrict(d.getDistrictId());
        }
        return population;
    }

    public  double populationDistrict(Integer districtID){
        double population = 0;
        DataSingleton ds = DataSingleton.getInstance();
        for (Precinct p: this.precinctData.get(districtID)) {
            population += ds.getPopulation(p.getPrecinctId()).getPopulation();
        }
        return population;
    }

    public static void main(String [] args){
        DataSingleton ds = DataSingleton.getInstance();
        StateID stateID = StateID.NE;
        ds.makeSureDataExist(stateID);
        GeneratedState gs = new GeneratedState(stateID);
        gs.copyAllData();
    }
}
