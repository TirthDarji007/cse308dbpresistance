//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(
        name = "POPULATION"
)
public class PopulationDolphin implements Serializable {
    @Id
    @GeneratedValue
    @Column(
            name = "POPULATION_ID"
    )
    private int poputationId;
    private Date date;
    private double population;
    @Column(
            name = "PRECINCT_ID"
    )
    private int precinctId;
    @Column(
            name = "DISTRICT_ID"
    )
    private int districtId;

    public PopulationDolphin(Date date, double population, int precinctId, int districtId) {
        this.date = date;
        this.population = population;
        this.precinctId = precinctId;
        this.districtId = districtId;
    }

    public int getPoputationId() {
        return this.poputationId;
    }

    public void setPoputationId(int poputationId) {
        this.poputationId = poputationId;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPopulation() {
        return this.population;
    }

    public void setPopulation(double population) {
        this.population = population;
    }

    public int getPrecinctId() {
        return this.precinctId;
    }

    public void setPrecinctId(int precinctId) {
        this.precinctId = precinctId;
    }

    public int getDistrictId() {
        return this.districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public PopulationDolphin() {
    }
}
